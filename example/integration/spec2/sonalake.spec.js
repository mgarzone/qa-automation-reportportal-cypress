context("Check content of Sonalake", () => {

  Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })


  const MENU_ITEM_HOME_SELECTOR = '[id="menu-item-1032"]';
  const MENU_ITEM_ABOUT_SELECTOR = '[id="menu-item-1094"]';
  const MENU_ITEM_SERVICES_SELECTOR = '[id="menu-item-1151"]';
  const MENU_ITEM_OUR_WORK_SELECTOR = '[id="menu-item-1109"]';
  const MENU_ITEM_CAREERS_SELECTOR = '[id="menu-item-1351"]';
  const MENU_ITEM_NEWS_BLOG_SELECTOR = '[id="menu-item-1368"]';

  const MENU_ITEM_HOME = "HOME";
  const MENU_ITEM_ABOUT = "ERROR ABOUT";
  const MENU_ITEM_SERVICES = "SERVICES";
  const MENU_ITEM_OUR_WORK = "OUR WORK";
  const MENU_ITEM_CAREERS = "CAREERS";
  const MENU_ITEM_NEWS_BLOG = "NEWS & BLOG";

  const PAGE_TITLE_HOME = "Sonalake - Your Software Innovation Partner";
  const PAGE_TITLE_ABOUT = "ERROR About Sonalake - software product development, software engineering";
  const PAGE_TITLE_SERVICES = "Services - Sonalake";
  const PAGE_TITLE_OUR_WORK = "Our work - examples of our customer engagements and projects";
  const PAGE_TITLE_CAREERS = "Software Development Careers at Sonalake";
  const PAGE_TITLE_NEWS_BLOG =  "Latest @ Sonalake - Visual Analytics, Software development and IT monitoring";


  beforeEach(() => {
    cy.visit("https://sonalake.com/");
  });
  it("url should contain sonalake", function() {
    cy.url().should("include", "sonalake");
  });
  
  it("check contains menu item HOME", () => {
    cy.contains(MENU_ITEM_HOME);
  });
  it("check contains menu item ERROR ABOUT  -failed", () => {
    cy.contains(MENU_ITEM_ABOUT).screenshot();
  });
  it("check contains menu item SERVICES", () => {
    cy.contains(MENU_ITEM_SERVICES);
  });
  it("check contains menu item OUR WORK", () => {
    cy.contains(MENU_ITEM_OUR_WORK);
  });
  it("check contain menu item CAREERS", () => {
    cy.contains(MENU_ITEM_CAREERS);
  });
  it("check contain menu item NEWS & BLOG", () => {
    cy.contains(MENU_ITEM_NEWS_BLOG);
  });

  it("check page title of SERVICES", () => {
    cy.get(MENU_ITEM_SERVICES_SELECTOR).click();
    cy.title().should('eq', PAGE_TITLE_SERVICES);
  });
  
  it("check page title of ABOUT -failed", () => {
    cy.get(MENU_ITEM_ABOUT_SELECTOR).click();
    cy.title().should('eq', PAGE_TITLE_ABOUT).screenshot();
  });
  it("check page title of OUR WORK", () => {
    cy.get(MENU_ITEM_OUR_WORK_SELECTOR).click();
    cy.title().should('eq', PAGE_TITLE_OUR_WORK);
  
  });
  it("check page title of CAREERS", () => {
    cy.get(MENU_ITEM_CAREERS_SELECTOR).click();
    cy.title().should('eq', PAGE_TITLE_CAREERS);
  });
  it("heck page title of NEWS & BLOG", () => {
    cy.get(MENU_ITEM_NEWS_BLOG_SELECTOR).click()
    cy.title().should('eq', PAGE_TITLE_NEWS_BLOG);
  });
  it("heck page title of HOME", () => {
    cy.get(MENU_ITEM_HOME_SELECTOR).click();
    cy.title().should('eq', PAGE_TITLE_HOME);
   });
 
});
